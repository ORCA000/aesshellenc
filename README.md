# AESShellEnc: 
#### its just a code to encrypt your shellcode ;p


# USAGE:
* first of all copy and paste your shellcode [here](https://gitlab.com/ORCA666/aesshellenc/-/blob/main/AESShellEnc/main.c#L20)
* change the [key](https://gitlab.com/ORCA666/aesshellenc/-/blob/main/AESShellEnc/main.c#L84) && the initialization vector [here](https://gitlab.com/ORCA666/aesshellenc/-/blob/main/AESShellEnc/main.c#L86)
* make sure your iv and key are 16 / 32 bytes long respectively
* now all u have to do is run the exe, if everything went will, u will have your encrypted shellcode printed.
* i added another project, to test the output, just paste everything in, and it will run your shellcode

# THANKS FOR:
* [tiny-AES-c](https://github.com/kokke/tiny-AES-c) 
* [@CaptMeelo](https://twitter.com/CaptMeelo?s=20&t=566_vB8-aOnbTgNzOgDNOw) for this nice [blog](https://captmeelo.com/redteam/maldev/2022/02/16/libraries-for-maldev.html) that discussed more things to mess with, including the implementation of tiny-AES-c as a functionality to do shellcode encryption.

# DEMO:
![image3](https://gitlab.com/ORCA666/aesshellenc/-/raw/main/images/image_3.png)

#### Note: This is a very basic way to do it, and u can probably see tons of other repos, but if u liked it, then ilu ;p. For curious ppl, u can add a function that reads a .bin file directly, use smth like `CreateFile` && `ReadFile` to read the shellcode, and u are good to go ... 


<h6 align="center"> <i>#                                   STAY TUNED FOR MORE</i>  </h6> 
![120064592-a5c83480-c075-11eb-89c1-78732ecaf8d3](https://gitlab.com/ORCA666/kcthijack/-/raw/main/images/PP.png)

